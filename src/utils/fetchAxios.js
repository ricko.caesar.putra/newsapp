import axios from "axios"

const fetchAxios = ({type,url,data}) => {
    if (type == 'post') {
        return (
            axios.post(url, data)
        )
    }
    if (type == 'get') {
        return (
            axios.get(url)
        )
    }
}

export default fetchAxios
