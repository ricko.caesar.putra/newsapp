export const colors = {
    // default: '#e74c3c', //red
    default: '#3a32a8',
    disable: '#A5B1C2', //grey
    dark: '#474747a0', //dark
    white: '#ecf0f1a0',//white
    text: {
        default: '#7e7e7e'//grey
    }
};
