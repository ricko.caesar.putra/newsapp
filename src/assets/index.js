//illustration
import welcomeAuth from './illustrations/kendaraan.png';
import IllustrationRegister from './illustrations/register.svg';
import IllustrationLogin from './illustrations/Login.svg';
import Snowman from './illustrations/snowman.svg';
//icon
import IconBack from './icons/undraw_back_home_nl5c.svg';
//background
import BGI from './illustrations/background.jpg';

// images
import ImagePay from './images/pay.png';
import ImageTopup from './images/topup.png';
import ImageReward from './images/reward.png';
import ImageFood from './images/food.png';
import ImageCar from './images/car.png';
import ImageBike from './images/bike.png';
import ImageDelivery from './images/delivery.png';
import ImageSubscription from './images/reward.png';
import ImageHealth from './images/doctor.png';
import ImageEmoney from './images/emoney.png';
import ImageMore from './images/more.png';
import ImageAlaska1 from './images/alaska1.jpg';
import ImageAlaska2 from './images/alaska2.jpg';
import ImageAlaska3 from './images/alaska3.jpg';
import ImageHome from './images/home.png';
import ImageActivity from './images/activity.png';
import ImageInbox from './images/inbox.png';
import ImagePayment from './images/payment.png';
import ImageAccount from './images/account.png';
import ImageHotel from './images/hotel.png';
import ImageWisata from './images/wisata.png';
import ImageTG from './images/tourGuide.png';

export {ImageTG,ImageHotel, ImageWisata, welcomeAuth, IconBack, IllustrationRegister, IllustrationLogin, Snowman, BGI,ImagePay,ImageTopup,ImageReward,ImageFood,ImageCar,ImageBike,ImageDelivery,ImageSubscription,ImageHealth,ImageEmoney,ImageMore,ImageAlaska1,ImageAlaska2,ImageAlaska3,ImageHome,ImageActivity,ImageInbox,ImagePayment,ImageAccount };
