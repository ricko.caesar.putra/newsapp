import firebase from 'firebase'

firebase.initializeApp({
    apiKey: "AIzaSyAZLY4aLj_BGFBU7blwwOwckofrSORKdvU",
    authDomain: "awesomeproject-b35e8.firebaseapp.com",
    projectId: "awesomeproject-b35e8",
    storageBucket: "awesomeproject-b35e8.appspot.com",
    messagingSenderId: "659856497103",
    appId: "1:659856497103:web:57114489ba79f8e5311aba"
})

const FIREBASE = firebase;

export default FIREBASE;