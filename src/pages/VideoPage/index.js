import React, { useState } from 'react';
import { StyleSheet, Text, View, ScrollView, TextInput, FlatList, ActivityIndicator, Animated, Keyboard, Dimensions } from 'react-native';
import VideoCard from '../../components/VideoCard'
import Icon from 'react-native-vector-icons/Ionicons'
import Config from "react-native-config"
import { colors } from '../../utils';
import fetchAxios from '../../utils/fetchAxios';

const { width, height } = Dimensions.get('screen');

const VideoPage = ({ navigation }) => {
    const [value, setValue] = useState("")
    const [miniCardData, setMiniCard] = useState([])
    const [loading, setLoading] = useState(false)
    // const scrollX=React.useRef(new Animated.value(0)).current;
    

    const fetchData = () => {
        setLoading(true)
        Keyboard.dismiss()
        const url=`https://youtube.googleapis.com/youtube/v3/search?part=snippet&maxResults=10&q=${value}&type=video&key=AIzaSyC30Y35Ir034Z69D61re6k9gzFLg6u3xJU`
        // fetchAxios(type='get',url=url)
        fetch(url)
            .then(res => res.json())
            .then(data => {
                setLoading(false)
                setMiniCard(data.items)
                // {!data.items?console.warn(data.message):null}
            })
    }

    return (
        <View style={{
            flex: 1,
        }}>
            <View style={styles.navigasiAtas}>
                <Icon name='md-arrow-back' size={32} color={colors.default} onPress={() => navigation.goBack()} />
                <TextInput
                    style={styles.textSearch}
                    value={value}
                    onChangeText={(text) => setValue(text)}
                />
                <Icon name='md-send' size={32} color={colors.default} onPress={() => fetchData()} />
            </View>

            {loading ? <ActivityIndicator style={{ marginTop: 10 }} size="large" color={colors.default} /> : 
            <Text style={{textAlign:'center', fontSize:20,fontWeight:'bold',color:'red'}} >swipe left/right to change</Text>
            }

            
            {/* <Icon name='md-youtube' size={32} color={'red'} onPress={() => fetchData()} /> */}
                    

            <Animated.FlatList
                data={miniCardData}
                horizontal
                pagingEnabled
                style={{ flexGrow: 0 }}
                showsHorizontalScrollIndicator={false}
                contentContainerStyle={{alignItems:'center'}}
                snapToInterval={width}
                decelerationRate={0}
                bounces={false}
                // onEndReached= {console.warn('botom')}

                // onScroll={Animated.event([{nativeEvent:{contentOffset:{x:scrollX}}}],{useNativeDriver:true})}
                // scrollEventThrottle={16}
                renderItem={({ item,index }) => {
                    return <VideoCard
                        title={item.snippet.title}
                        source={item.snippet.channelId}
                        description={item.snippet.description}
                        url={`https://www.youtube.com/embed/${item.id.videoId}`}
                        imageMC={item.snippet.thumbnails.high.url}
                        publishedAt={item.snippet.publishTime}
                        index={index}
                    />
                }}
                keyExtractor={item =>item.id.videoId}
            />

            {/* <View style={{height:10}}/> */}

        </View>
    )
}

export default VideoPage

const styles = StyleSheet.create({
    navigasiAtas:{
        padding: 5,
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems:'center',
        elevation: 5,
        backgroundColor: 'white',
        marginTop: 30
    },
    textSearch:{
        width: "70%",
        height: 30,
        backgroundColor: colors.dark,
        padding:5,
        fontSize:17,
        fontWeight:'bold'
    }

  
  })
  
  