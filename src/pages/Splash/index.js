import React from 'react'
import { useEffect } from 'react'
import { ImageBackground, StyleSheet, Text, View } from 'react-native'
import { BGI, Snowman } from '../../assets';
import { colors } from '../../utils';
import AsyncStorage from '@react-native-async-storage/async-storage'


const Splash = ({ navigation }) => {
    useEffect(() => {
        setTimeout(() => {
            // navigation.replace('WelcomeAuth');
            const getData = async () => {
                try {
                  const jsonValue = await AsyncStorage.getItem('@token')
                  navigation.navigate(jsonValue?'BotomTab':'WelcomeAuth')               
                } catch(e) {
                  console.warn(e)
                }
              }
    
            getData();
        }, 2000);
    });
    return (
        <ImageBackground source={BGI} style={styles.background}>
            <Snowman width={250} height={250} />
            <Text style={styles.text}>Alaska</Text>
        </ImageBackground>
    )
}

export default Splash

const styles = StyleSheet.create({
    background: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    text: {
        fontSize: 70,
        fontWeight: 'bold',
        fontStyle: 'italic',
        color: colors.default
    }
})
