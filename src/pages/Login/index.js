import React, { useState } from 'react'
import { ImageBackground, Text, View } from 'react-native'
import { Button, Input } from '../../components'
import { colors } from '../../utils'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import { BGI, Snowman } from '../../assets'
import { useDispatch, useSelector } from 'react-redux'
import { setForm } from '../../redux'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'


const Login = ({ navigation }) => {
    const { form } = useSelector(state => state.LoginReducer);
    const dispatch = useDispatch();
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const formLogin =
    {
        "email": email,
        "password": password
    }

    const sendData = async () => {
        // console.log('data yang dikirim: ', form);
        // alert(`email: ${form.email}, password: ${form.password}`)

        // if (form.email === "FIREBASE" && form.password === "FIREBASE") {
        //     navigation.navigate("Admin")
        // }
        // if (form.email === "JSONServer" && form.password === "JSONServer") {
        //     navigation.navigate("AdminJSONServer")
        // }

        // console.warn(formLogin)


        try {
            axios.post('https://api-nodejs-todolist.herokuapp.com/user/login', formLogin)
                .then(function (response) {
                    if (response.data.token != null) {
                        AsyncStorage.setItem('@token', response.data.token);
                        navigation.navigate('BotomTab')

                    } else {
                        alert('error')
                    }
                })
                .catch(function (error) {
                    console.warn(error);
                });

            // let fetchJson = await fetch('https://api-nodejs-todolist.herokuapp.com/user/login', {
            //     method: 'POST',
            //     headers: {
            //         Accept: 'application/json',
            //         'Content-Type': 'application/json',
            //     },
            //     body: JSON.stringify(formLogin),
            // });

            // let response = await fetchJson.json();
            // console.warn(response.token)
            // console.log(response)

            // if (response.token != null) {
            //     AsyncStorage.setItem('@token', response.token);
            //     navigation.navigate('BotomTab')

            // } else {
            //     alert('error')
            // }

        } catch (error) {
            console.error(error)
        }

    }

    const onInputChange = (value, inputType) => {
        dispatch(setForm(inputType, value))
    }

    return (
        <ImageBackground source={BGI} style={styles.wrapper.page}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <Button type="icon" name="back" onPress={() => navigation.goBack()} />
                <View style={{ alignItems: 'center' }}>
                    <Snowman width={106} height={115} style={styles.illustration} />
                    <Text
                        style={styles.text.desc}>
                        Silahkan mengisi data anda
            </Text>
                </View>
                <View style={styles.space(50)} />
                <Input
                    placeholder="email"
                    // value={formLogin.email}
                    value={email}
                    // onChangeText={(value) => onInputChange(value, 'email')
                    onChangeText={(value) => { setEmail(value) }
                    }
                />
                <View style={styles.space(10)} />
                <Input
                    placeholder="password"
                    value={formLogin.password}
                    value={password}
                    // onChangeText={(value) => onInputChange(value, 'password')}
                    onChangeText={(value) => { setPassword(value) }}
                    secureTextEntry={true}
                />
                <View style={styles.space(177)} />
                <Button title="Login" onPress={sendData} />
                <Text
                    style={styles.text.konfirmasi}>
                    belum punya Account?
            </Text>
                <TouchableOpacity onPress={() => navigation.navigate("Register")}>
                    <Text
                        style={styles.text.opsi}>
                        DAFTAR
            </Text>
                </TouchableOpacity>
            </ScrollView>
        </ImageBackground>
    )
}

export default Login

const styles = {
    wrapper: {
        page: {
            padding: 20,
            flex: 1,
        }
    },
    iconBack: {
        width: 25,
        height: 25,
        backgroundColor: 'blue'
    },
    illustration: {
        width: 106,
        height: 115,
        marginTop: 8,

    },
    text: {
        desc: {
            fontSize: 16,
            fontWeight: 'bold',
            color: colors.default,
            marginTop: 16,
        },
        konfirmasi: {
            textAlign: 'center',
            color: colors.default,
            fontWeight: 'bold',
            fontSize: 16
        },
        opsi: {
            textAlign: 'center',
            color: 'red',
            borderRadius: 10,
            fontWeight: 'bold',
            fontSize: 18
        }
    },
    space: value => {
        return {
            height: value,
        };
    }

}