import React, { useState } from 'react';
import { StyleSheet, Text, View, FlatList, ActivityIndicator, Dimensions } from 'react-native';
import MiniCard from '../../components/MiniCard'
import Icon from 'react-native-vector-icons/Ionicons'
import { useEffect } from 'react';
import HeadLineCard from '../../components/HeadLineCard';
import Config from "react-native-config";
import { colors } from '../../utils';
import { ScrollView } from 'react-native-gesture-handler';
import { Button } from '../../components';

const { width, height } = Dimensions.get('screen');

const News = ({ navigation }) => {
    const [miniCardData, setMiniCard] = useState([])
    const [loading, setLoading] = useState(false)
    const [page, setPage] = useState(1)
    const [cat, setCat] = useState('business')

    useEffect(() => {
        fetchData();
    }, [page,cat])

    const fetchData = () => {
        setLoading(true)
        // fetch(`https://newsapi.org/v2/top-headlines?country=id&apiKey=2bdaa5281f784c4fa9ef7d8b43e22f9b&page=${page}&pageSize=10&category=${cat}`)
        fetch(`https://newsapi.org/v2/top-headlines?country=id&apiKey=${Config.API}&page=${page}&pageSize=10&category=${cat}`)
            .then(res => res.json())
            .then(data => {
                setLoading(false)
                { !data.articles ? console.warn(data.message): setMiniCard(miniCardData.concat(data.articles)) } 
                // setMiniCard(data.articles)
                              
            })
    }

    const handleLoadMore = () => {
        setPage(page + 1)        
    }

    const pilih = (opsi) => {        
        setMiniCard([])
        setCat(opsi)
    }

    return (
        <View style={{ flex: 1, }}>
            <View style={styles.navigasiAtas}>
                <Text style={styles.textHeadline}>
                    Headlines News
                </Text>

                <Icon name='md-search' size={32} color={colors.default} onPress={() => navigation.navigate('SearchNews')} />
            </View>

            <ScrollView horizontal>
                <Button type='headlines' title='business' onPress={() => pilih('business')} />
                <Button type='headlines' title='entertainment' onPress={() => pilih('entertainment')} />
                <Button type='headlines' title='general' onPress={() => pilih('general')} />
                <Button type='headlines' title='health' onPress={() => pilih('health')} />
                <Button type='headlines' title='science' onPress={() => pilih('science')} />
                <Button type='headlines' title='sports' onPress={() => pilih('sports')} />
                <Button type='headlines' title='technology' onPress={() => pilih('technology')} />
            </ScrollView>

            <FlatList
                // style={{ flex: 1 }}
                // contentContainerStyle={{ flex: 1 }}
                data={miniCardData}
                renderItem={({ item, index }) => {
                    if (index == 0) {
                        return <HeadLineCard
                            index={index}
                            title={item.title}
                            source={item.source.name}
                            description={item.description}
                            url={item.url}
                            imageMC={item.urlToImage}
                            publishedAt={item.publishedAt}
                        />
                    } else {
                        return <MiniCard
                            index={index}
                            title={item.title}
                            source={item.source.name}
                            description={item.description}
                            url={item.url}
                            imageMC={item.urlToImage}
                            publishedAt={item.publishedAt}
                        />

                    }
                }}
                keyExtractor={(item) => item.url}
                onEndReached={()=>handleLoadMore()}
                onEndReachedThreshold={0.3}
            />

            {loading ? <ActivityIndicator style={{ marginTop: 10 }} size="large" color={colors.default} /> : null}

            <View style={{ height: 10 }} />
        </View>
    )
}

export default News

const styles = StyleSheet.create({
    navigasiAtas: {
        padding: 5,
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: 'center',
        elevation: 5,
        backgroundColor: 'white',
        marginTop: 30
    },
    textHeadline: {
        width: "70%",
        height: 35,
        padding: 3,
        fontSize: 25,
        fontWeight: 'bold'
    }
})