import React from 'react'
import { ImageBackground, Text, View } from 'react-native'
import { Button, Input } from '../../components'
import { colors } from '../../utils'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import { BGI, Snowman } from '../../assets'
import { useDispatch, useSelector } from 'react-redux'
import { setForm } from '../../redux'
import FIREBASE from '../../config/FIREBASE'
import axios from 'axios'


const Register = ({ navigation }) => {
    const { form } = useSelector(state => state.RegisterReducer);
    const dispatch = useDispatch();

    const sendData = async () => {
        if (form.name && form.email && form.age && form.password) {
            // console.warn(form)

        try {
            axios.post('https://api-nodejs-todolist.herokuapp.com/user/register', form)
            .then(function (response) {
                console.warn(response)
                if (response.data != null) {
                    navigation.navigate('Login')
                } else {
                    alert('error')
                }
            })
            .catch(function (error) {
                console.log(error);
            });
            
            // let fetchJson = await fetch('https://api-nodejs-todolist.herokuapp.com/user/register', {
            //     method: 'POST',
            //     headers: {
            //         Accept: 'application/json',
            //         'Content-Type': 'application/json',
            //     },
            //     body: JSON.stringify(form),
            // });
       
            // let response = await fetchJson.json();
            // // console.warn(response)
            // navigation.navigate('Login')

        }catch(error){
            console.error(error)
        }

            // //kirim ke fakeAPI
            // axios.post('http://10.0.2.2:3000/DataUser', form)
            //     .then(res => {
            //         console.log('res: ', res);
            //     })

            // // kirim ke firebase
            // const registerReferensi = FIREBASE.database().ref('DataUser');
            // registerReferensi
            //     .push(form)
            //     .then((data) => {
            //         alert('sukses', 'data tersimpan');
            //         navigation.navigate("Login");
            //     })
            //     .catch((error) => {
            //         console.log('error: ', error)
            //     })

        } else {
            alert('lengkapi data!')
        };
    }

    const onInputChange = (value, inputType) => {
        dispatch(setForm(inputType, value))
    }

    return (
        <ImageBackground source={BGI} style={styles.wrapper.page}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <Button type="icon" name="back" onPress={() => navigation.goBack()} />
                <View style={{ alignItems: 'center' }}>
                    <Snowman width={106} height={115} style={styles.illustration} />
                    <Text
                        style={styles.text.desc}>
                        Mohon mengisi beberapa data untuk proses anda
                        </Text>
                </View>
                <View style={styles.space(50)} />
                <Input
                    placeholder="name"
                    value={form.name}
                    onChangeText={(value) => onInputChange(value, 'name')} />
                <View style={styles.space(10)} />
                <Input
                    placeholder="email"
                    value={form.email}
                    onChangeText={(value) => onInputChange(value, 'email')}
                />
                <View style={styles.space(10)} />
                <Input
                    placeholder="age"
                    value={form.age}
                    onChangeText={(value) => onInputChange(value, 'age')}
                />
                <View style={styles.space(10)} />
                <Input
                    placeholder="password"
                    value={form.password}
                    onChangeText={(value) => onInputChange(value, 'password')}
                    secureTextEntry={true}
                />
                <View style={styles.space(54)} />
                <Button title="Daftar" onPress={sendData} />
                <Text
                    style={styles.text.konfirmasi}>
                    sudah punya Account?
            </Text>
                <TouchableOpacity onPress={() => navigation.navigate("Login")}>
                    <Text
                        style={styles.text.opsi}>
                        LOGIN
            </Text>
                </TouchableOpacity>
            </ScrollView>
        </ImageBackground>
    )
}

export default Register

const styles = {
    wrapper: {
        page: {
            padding: 20,
            flex: 1,
        }
    },
    illustration: {
        width: 106,
        height: 115,
        marginTop: 8,

    },
    text: {
        desc: {
            fontSize: 16,
            fontWeight: 'bold',
            color: colors.default,
            marginTop: 16,
        },
        konfirmasi: {
            textAlign: 'center',
            color: colors.default,
            fontWeight: 'bold',
            fontSize: 16
        },
        opsi: {
            textAlign: 'center',
            color: 'red',
            borderRadius: 10,
            fontWeight: 'bold',
            fontSize: 18
        }
    },
    space: value => {
        return {
            height: value,
        };
    }

}
