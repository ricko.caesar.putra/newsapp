import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, Text, Alert } from 'react-native';
import { InputData } from '../../components';
import FIREBASE from '../../config/FIREBASE'

export default class EditUser extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fullName: '',
            email: '',
            contactNumber: '',
            password: '',
        };
    }

    componentDidMount() {
        FIREBASE.database()
            .ref('DataUser/' + this.props.route.params.id)
            .once('value', (querySnapShot) => {
                let data = querySnapShot.val() ? querySnapShot.val() : {};
                let kontakItem = { ...data };

                this.setState({
                    fullName: kontakItem.fullName,
                    email: kontakItem.email,
                    contactNumber: kontakItem.contactNumber,
                    password: kontakItem.password
                });
            });
    }

    onChangeText = (namaState, value) => {
        this.setState({
            [namaState]: value,
        });
    };

    onSubmit = () => {
        if (this.state.fullName && this.state.email && this.state.contactNumber && this.state.password) {

            const kontakReferensi = FIREBASE.database().ref('DataUser/' + this.props.route.params.id);

            const kontak = {
                fullName: this.state.fullName,
                email: this.state.email,
                contactNumber: this.state.contactNumber,
                password: this.state.password
            }

            kontakReferensi
                .update(kontak)
                .then((data) => {
                    Alert.alert('Sukses', 'Kontak Terupdate');
                    this.props.navigation.replace('Admin');
                })
                .catch((error) => {
                    console.log("Error : ", error);
                })


        } else {
            Alert.alert('Error', 'semua data wajib diisi');
        }

    };

    render() {
        return (
            <View style={styles.pages}>
                <InputData
                    label="fullName"
                    placeholder="Masukkan Nama"
                    onChangeText={this.onChangeText}
                    value={this.state.fullName}
                    namaState="fullName"
                />
                <InputData
                    label="email"
                    placeholder="Masukkan email"
                    onChangeText={this.onChangeText}
                    value={this.state.email}
                    namaState="email"
                />

                <InputData
                    label="contactNumber"
                    placeholder="Masukkan nomor telepon"
                    keyboardType="number-pad"
                    onChangeText={this.onChangeText}
                    value={this.state.contactNumber}
                    namaState="contactNumber"
                />

                <InputData
                    label="password"
                    placeholder="Masukkan password"
                    isTextArea={true}
                    onChangeText={this.onChangeText}
                    value={this.state.password}
                    namaState="password"
                />

                <TouchableOpacity style={styles.tombol} onPress={() => this.onSubmit()}>
                    <Text style={styles.textTombol}>SUBMIT</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    pages: {
        flex: 1,
        padding: 30,
    },
    tombol: {
        backgroundColor: 'black',
        padding: 10,
        borderRadius: 5,
        marginTop: 10,
    },
    textTombol: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 16,
    },
});
