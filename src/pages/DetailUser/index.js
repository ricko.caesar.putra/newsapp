import React, {Component} from 'react';
import {Text, StyleSheet, View} from 'react-native';
import FIREBASE from '../../config/FIREBASE';

export default class DetailUser extends Component {
  constructor(props) {
    super(props);

    this.state = {
      kontak: {},
    };
  }

  componentDidMount() {
    FIREBASE.database()
      .ref('DataUser/'+ this.props.route.params.id)
      .once('value', (querySnapShot) => {
        let data = querySnapShot.val() ? querySnapShot.val() : {};
        let kontakItem = {...data};

        this.setState({
          kontak: kontakItem,
        });
      });
  }

  render() {
    const {kontak} = this.state;
    return (
      <View style={styles.pages}>
        <Text>Name : </Text>
        <Text style={styles.text}>{kontak.fullName} </Text>

        <Text>Email : </Text>
        <Text style={styles.text}>{kontak.email} </Text>

        <Text>Contact Number : </Text>
        <Text style={styles.text}>{kontak.contactNumber} </Text>

        <Text>Password : </Text>
        <Text style={styles.text}>{kontak.password} </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    pages : {
        margin: 30,
        padding: 20,
        backgroundColor: 'white',
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
    
        elevation: 5,
    },
    text: {
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 10   
    }
});
