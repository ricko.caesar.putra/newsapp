import Splash from "./Splash";
import Login from "./Login";
import Register from "./Register";
import WelcomeAuth from "./WelcomeAuth";
import Admin from "./Admin";
import DetailUser from "./DetailUser";
import EditUser from "./EditUser"
import AdminJSONServer from "./AdminJSONServer"
import Account from "./Account"
import Home from './Home'
import News from './News'
import VideoPage from './VideoPage'
import Food from './Food'
import Payment from './Payment'
import SearchNews from './SearchNews'
import DisplayWeb from './DisplayWeb'

export{Splash,Login,Register,WelcomeAuth,Admin,DetailUser,EditUser,AdminJSONServer,Account,Home,VideoPage,Food,News,Payment,SearchNews,DisplayWeb};


