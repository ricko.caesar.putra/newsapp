import React, { useState } from 'react';
import { StyleSheet, Text, View, ScrollView, TextInput, FlatList, ActivityIndicator, Animated, Keyboard } from 'react-native';
import MiniCard from '../../components/MiniCard'
import Icon from 'react-native-vector-icons/Ionicons'
import Config from "react-native-config"
import { colors } from '../../utils';

const SearchNews = ({ navigation }) => {
    const [value, setValue] = useState("")
    const [miniCardData, setMiniCard] = useState([])
    const [loading, setLoading] = useState(false)

    const fetchData = () => {
        setLoading(true)
        Keyboard.dismiss()

        // fetch(`https://newsapi.org/v2/everything?q=${value}&apiKey=2bdaa5281f784c4fa9ef7d8b43e22f9b`)
        fetch(`https://newsapi.org/v2/everything?q=${value}&apiKey=${Config.API}`)
            .then(res => res.json())
            .then(data => {
                setLoading(false)
                { !data.articles ? console.warn(data.message): setMiniCard(data.articles) }                 
                // {!data.articles?console.warn(data.message):null}
            })
    }

    return (
        <View style={{
            flex: 1,
        }}>
            <View style={styles.navigasiAtas}>
                <Icon name='md-arrow-back' size={32} color={colors.default} onPress={() => navigation.goBack()} />
                <TextInput
                    style={styles.textSearch}
                    value={value}
                    onChangeText={(text) => setValue(text)}
                />
                <Icon name='md-send' size={32} color={colors.default} onPress={() => fetchData()} />
            </View>

            {loading ? <ActivityIndicator style={{ marginTop: 10 }} size="large" color={colors.default} /> : null}

            <FlatList
                data={miniCardData}
                // horizontal
                // pagingEnabled
                bounces={false}
                style={{ flexGrow: 0 }}
                // onScroll={Animated.event([{nativeEvent:{contentOffset:{x:scrollX}}}],{useNativeDriver:true})}
                renderItem={({ item,index }) => {
                    return <MiniCard
                        title={item.title}
                        source={item.source.name}
                        description={item.description}
                        url={item.url}
                        imageMC={item.urlToImage}
                        publishedAt={item.publishedAt}
                        index={index}
                    />
                }}
                keyExtractor={item => item.url}
            />
        </View>
    )
}

export default SearchNews

const styles = StyleSheet.create({
    navigasiAtas:{
        padding: 5,
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems:'center',
        elevation: 5,
        backgroundColor: 'white',
        marginTop: 30
    },
    textSearch:{
        width: "70%",
        height: 30,
        backgroundColor: colors.dark,
        padding:5,
        fontSize:17,
        fontWeight:'bold'
    }

  
  })
  
  