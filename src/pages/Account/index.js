import AsyncStorage from '@react-native-async-storage/async-storage'
import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View,ImageBackground } from 'react-native'
import { BGI} from '../../assets'
import { colors } from '../../utils'
import { useEffect } from 'react'

const Account = ({navigation}) => {
    const _logout=async ()=>{
        await AsyncStorage.clear()
        navigation.navigate('Login')
    }

    // useEffect(() => {
    //     setTimeout(() => {
    //         // navigation.replace('WelcomeAuth');
    //         const getData = async () => {
    //             try {
    //               const jsonValue = await AsyncStorage.getItem('@token')
    //               return jsonVlue             
    //             } catch(e) {
    //               // error reading value
    //             }
    //           }
            
    //     },[]);
    // });

    return (
        <ImageBackground source={BGI} style={styles.background}>
        <View style={{flex:1,justifyContent:'center'}}>
            <TouchableOpacity 
            onPress={_logout}
            style={{
                backgroundColor:colors.default,
                height:40,
                width:200,
                alignSelf:'center',
                justifyContent:'center',
                borderRadius:20
            }}>
                <Text style={{
                    fontSize:20,
                    alignSelf:'center',
                    color:'white',
                    fontWeight:'bold'
                    
                }}>Logout</Text>
            </TouchableOpacity>
        </View>        
        </ImageBackground>

    )
}

export default Account

const styles = StyleSheet.create({
    background: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
})
