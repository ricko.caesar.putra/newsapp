import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Alert, StyleSheet, Text, TextInput, View, Image, TouchableOpacity, ImageBackground } from 'react-native'
import { Button, Input } from '../../components'
import { ScrollView } from 'react-native-gesture-handler'
import { BGI } from '../../assets'
import { colors } from '../../utils'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faHome } from '@fortawesome/free-solid-svg-icons'

const Item = ({ fullName, email, contactNumber, password, onPress, onDelete }) => {
    return (
        <View style={styles.itemContainer}>
            <View style={{ alignItems: "center" }}>
                <TouchableOpacity onPress={onPress}>
                    <Image source={{ uri: `https://i.pravatar.cc/150?u=${fullName}` }}
                        style={styles.avatar} />
                </TouchableOpacity>
                <TouchableOpacity onPress={onDelete}>
                    <Text style={styles.delete}>Delete</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.desc}>
                <Text style={styles.descName}>Name: {fullName}</Text>
                <Text style={styles.descEmail}>email: {email}</Text>
                <Text style={styles.descEmail}>contact number: {contactNumber}</Text>
                <Text style={styles.descPassword}>password: {password}</Text>
            </View>
        </View>
    )
}

const AdminJSONServer = ({ navigation }) => {
    const [fullName, setFullName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [contactNumber, setContactNumber] = useState("");
    const [users, setUsers] = useState([]);
    const [button, setButton] = useState("Simpan");
    const [selectedUser, setSelectedUser] = useState({})

    useEffect(() => {
        getData();
    }, [])

    const submit = () => {
        const data = {
            fullName,
            email,
            contactNumber,
            password
        }
        // console.log('data before send: ',data);
        if (button === 'Simpan') {
            axios.post('http://10.0.2.2:3000/DataUser', data)
                .then(res => {
                    console.log('res: ', res);
                    setFullName("");
                    setEmail("");
                    setContactNumber("");
                    setPassword("");
                    getData();
                })
        } else if (button === 'Update') {
            axios.put(`http://10.0.2.2:3000/DataUser/${selectedUser.id}`, data)
                .then(res => {
                    console.log('res update: ', res);
                    setFullName("");
                    setEmail("");
                    setContactNumber("");
                    setPassword("");
                    getData();
                    setButton("Simpan");
                })
        }
    }

    const getData = () => {
        axios.get('http://10.0.2.2:3000/DataUser')
            .then(res => {
                console.log('res get data: ', res);
                setUsers(res.data);
            })
    }

    const selectItem = (item) => {
        console.log('selected item :', item);
        setSelectedUser(item);
        setFullName(item.fullName);
        setEmail(item.email);
        setContactNumber(item.contactNumber);
        setPassword(item.password)
        setButton("Update");
    }

    const deleteItem = (item) => {
        console.log(item)
        axios.delete(`http://10.0.2.2:3000/DataUser/${item.id}`)
            .then(res => {
                console.log('res delete: ', res);
                getData();
            })
    }

    return (
        <ImageBackground source={BGI} style={{
            padding: 20,
            flex: 1,
        }}>
            <ScrollView>
                <Text style={styles.textTitle}>Data User</Text>
                <View style={styles.wrapperButton}>
                    <TouchableOpacity
                        style={styles.btnHome}
                        onPress={() => navigation.navigate("WelcomeAuth")}>
                        <FontAwesomeIcon icon={faHome} size={20} color={'white'} />
                    </TouchableOpacity>
                </View>
                <Input placeholder="Full Name" value={fullName} onChangeText={(value) => setFullName(value)} />
                <View style={styles.space(3)} />
                <Input placeholder="Email" value={email} onChangeText={(value) => setEmail(value)} />
                <View style={styles.space(3)} />
                <Input placeholder="Contact Number" value={contactNumber} onChangeText={(value) => setContactNumber(value)} />
                <View style={styles.space(3)} />
                <Input placeholder="Password" value={password} onChangeText={(value) => setPassword(value)} />
                <View style={styles.space(10)} />
                <Button title={button} onPress={submit} />
                <View style={styles.line} />
                {users.map(user => {
                    return <Item
                        key={user.id}
                        fullName={user.fullName}
                        email={user.email}
                        contactNumber={user.contactNumber}
                        password={user.password}
                        onPress={() => selectItem(user)}
                        onDelete={() => Alert.alert(
                            'peringatan',
                            'anda yakin akan menghapus user ini?',
                            [
                                {
                                    text: 'Tidak',
                                    onPress: () => console.log('button tidak')
                                },
                                {
                                    text: 'Ya',
                                    onPress: () => deleteItem(user)
                                }
                            ])} />
                })}

            </ScrollView>
        </ImageBackground>
    )
}

export default AdminJSONServer

const styles = StyleSheet.create({
    textTitle: { textAlign: 'center', fontWeight: 'bold', margin: 10, fontSize: 20 },
    line: { height: 2, backgroundColor: 'black', marginVertical: 20 },
    avatar: { width: 80, height: 80, borderRadius: 20 },
    input: { borderWidth: 1, marginBottom: 12, borderRadius: 25, paddingHorizontal: 18 },
    itemContainer: { flexDirection: 'row', marginBottom: 20, borderRadius: 20, backgroundColor: colors.white, alignItems: "center", },
    desc: { marginLeft: 18, flex: 1 },
    descName: { fontSize: 20, fontWeight: 'bold' },
    descEmail: { fontSize: 16 },
    descPassword: { fontSize: 12, marginTop: 8 },
    delete: { fontSize: 20, fontWeight: 'bold', color: colors.default },
    wrapperButton: {
        position: 'absolute',
        top: 0,
        left: 0,
        margin: 0,
    },
    btnHome: {
        padding: 10,
        backgroundColor: colors.default,
        borderRadius: 30,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    space: value => {
        return {
            height: value,
        };
    }

})
