import React, { Component } from 'react'
import { Text, StyleSheet, View, Dimensions, StatusBar, Image } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import sky from '../../assets/images/sky.jpg'
import FiturUtama from '../../components/FiturUtama'
import PaymentComponents from '../../components/PaymentComponents'
import PromoItems from '../../components/PromoItems'
import { colors } from '../../utils'


const { height, width } = Dimensions.get('window')

export default class Home extends Component {
  render() {
    return (
      <ScrollView style={{ flex: 1, backgroundColor: 'white' }}>
        <StatusBar barStyle="dark-content" translucent backgroundColor="rgba(0,0,0,0)" />
        <Image style={styles.imageBanner} source={sky} />
        <Text style={styles.greetingText}>Alaska in Your Hand</Text>
        <View style={styles.devider}/>
        <View style={styles.wrapperAtas}>
          <View style={styles.textAtas}>
            <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#383838' }}>
            Alaska Balance
                        </Text>
            <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#383838' }}>
              Rp 1.000.000
                        </Text>
          </View>
          <View style={styles.garis}></View>
          <PaymentComponents />
        </View>
        <FiturUtama />
        <View style={styles.devider}/>
        <PromoItems/>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  imageBanner: {
    height: 140, width: width,
  },
  greetingText: {
    fontSize: 25,
    fontWeight: 'bold',
    position: 'absolute',
    alignSelf: 'center',
    top: 30,
    color: colors.default
  },
  wrapperAtas: {
    marginHorizontal: 18,
    height: 150,
    marginTop: -90,
    backgroundColor: "white",
    elevation: 10,
    borderRadius: 10,
    borderColor:colors.default,
    borderWidth:3
    
  },
  textAtas: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 12,
    marginTop: 10,
  },
  garis: {
    height: .8,
    backgroundColor: colors.default,
    marginTop: 10,
  },
  devider:{
    width:width,
    height:10,
    backgroundColor:colors.disable,
    marginVertical:10
  }

})

