import React from 'react'
import { ImageBackground, Text, View } from 'react-native'
import { BGI, Snowman } from '../../assets';
import { colors } from '../../utils';
import ActionButton from "../../components/ActionButton";


const WelcomeAuth = ({ navigation }) => {
    const handleGoTo = (screen) => {
        navigation.navigate(screen)
    }
    return (
        < ImageBackground source={BGI} style={styles.wrapper.page}>
            <View style={styles.space(100)} />
            <Snowman width={150} height={150} />
            <Text style={styles.text.welcome}>Selamat datang</Text>
            <Text style={styles.text.welcome}>Jalajahi Alaska dari Gadgetmu!</Text>
            <View style={styles.space(100)} />
            <ActionButton
                desc="Silahkan Masuk"
                title="masuk"
                onPress={() => handleGoTo('Login')}
            />
            <ActionButton
                desc="Belum Punya Account?"
                title="daftar"
                onPress={() => handleGoTo('Register')}
            />
        </ImageBackground>
    )
}

export default WelcomeAuth

const styles = {
    wrapper: {
        page: {
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'white',
            flex: 1
        },
    },
    text: {
        welcome: {
            fontSize: 20,
            fontWeight: 'bold',
            color: colors.default,
            marginBottom: 5,
            marginTop: 10,
        }

    },
    space: value => {
        return {
            height: value,
        };
    }
}