import { combineReducers } from "redux"

export const initialStateRegister = {
    form: {
        name: '',
        email: '',
        password: '',
        age: ''
    },
    title: 'register Page',
    desc: 'ini adalah desc untuk regester'
}

const RegisterReducer = (state = initialStateRegister, action) => {
    if (action.type === 'SET_TITLE') {
        return {
            ...state,
            title: 'register ganti title'
        }
    }
    if (action.type === 'SET_FORM') {
        return {
            ...state,
            form: {
                ...state.form,
                [action.inputType]: action.inputValue,
            }
        }
    }
    return state
}

export const initialStateLogin = {
    form: {
        email: '',
        password: '',
    },
    info: 'tolong masukkan password anda',
    isLogin: true,
}

const LoginReducer = (state = initialStateLogin, action) => {
    if (action.type === 'SET_FORM') {
        return {
            ...state,
            form: {
                ...state.form,
                [action.inputType]: action.inputValue,
            }
        }
    }
    return state
}

const reducer = combineReducers({
    RegisterReducer,
    LoginReducer,
})

export default reducer;