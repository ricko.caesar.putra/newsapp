import Button from './Button';
import Input from './input';
import Card from './Card';
import InputData from './InputData';

export {Button,Input,Card,InputData};