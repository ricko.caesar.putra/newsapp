import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { TextInput } from 'react-native-gesture-handler'
import { colors } from '../../../utils'


const Input = ({ placeholder, ...rest }) => {
    //kumpulkan semua props lainnya dengan nama rest
    return (
        <View>
            {/* <Text style={styles.label}>{placeholder} :</Text> */}
            <TextInput
                style={styles.input}
                placeholder={placeholder}
                placeholderTextColor={colors.default}
                {...rest}
            />
        </View>
    )
}

export default Input


const styles = StyleSheet.create({
    input: {
        borderWidth: 1,
        borderColor: colors.default,
        borderRadius: 25,
        paddingVertical: 12,
        paddingHorizontal: 18,
        fontSize: 14,
        backgroundColor: colors.white,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    label: {
        fontSize: 16,
        marginBottom: 5,
    }
})
