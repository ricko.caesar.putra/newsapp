import { faBackspace, faPlus } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import React from 'react'
import { StyleSheet } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { IconBack } from '../../../assets'

const ButtonIcon = ({ ...rest }) => {
    return (
        <TouchableOpacity {...rest}>
            {rest.name === 'back' && <FontAwesomeIcon icon={faBackspace} size={25} color={'black'} />}
            {rest.name === 'tambah' && <FontAwesomeIcon icon={faPlus} size={25} color={'black'} />}
        </TouchableOpacity>
    )
}

export default ButtonIcon

const styles = StyleSheet.create({})
