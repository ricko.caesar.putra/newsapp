import React from 'react'
import { StyleSheet, Text, } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { colors } from '../../../utils'
import ButtonIcon from './ButtonIcon'

const Button = ({ title, onPress, type, name }) => {
    if (type === "icon") {
        return (<ButtonIcon name={name} onPress={onPress} />)
    }
    if (type === "headlines") {
        return (<TouchableOpacity
            style={styles.wrapper.componentHeadlines}
            onPress={onPress}>
            <Text
                style={styles.text.title}>
                {title}
            </Text>
        </TouchableOpacity>)
    }
    return (
        <TouchableOpacity
            style={styles.wrapper.component}
            onPress={onPress}>
            <Text
                style={styles.text.title}>
                {title}
            </Text>
        </TouchableOpacity>
    )
}

export default Button

const styles = {
    wrapper: {
        component: {
            backgroundColor: colors.default,
            borderRadius: 25,
            paddingVertical: 13,
            // width:100,

        },
        componentHeadlines: {
            backgroundColor: colors.default,
            borderRadius: 25,
            paddingVertical: 10,
            width:110,
            marginLeft:5,
            marginBottom:15,
            marginTop:5

        },

    },
    text: {
        title: {
            fontSize: 12,
            fontWeight: 'bold',
            color: 'white',
            textTransform: 'uppercase',
            textAlign: 'center',
        }
    }
}