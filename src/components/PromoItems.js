import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { ImageAlaska1,ImageAlaska2,ImageAlaska3 } from '../assets'
import PromoItemsSub from './PromoItemsSub'

const PromoItems = () => {
    return (
        <View style={styles.wrapper}>
            <PromoItemsSub
                image={ImageAlaska1} text="Alaska 1"
                diskon="Diskon 40%"
                masaBerlaku="until 19 Mar"
                />
            <PromoItemsSub
                image={ImageAlaska2} text="Alaska 2" 
                masaBerlaku="until 19 Mar"
                />
            <PromoItemsSub
                image={ImageAlaska3} text="Alaska 3"
                masaBerlaku="until 19 Mar" 
                />
        </View>
    )
}

export default PromoItems

const styles = StyleSheet.create({
    wrapper: {
        width: '100%',
        flexWrap: 'wrap',
        flexDirection: 'row',
        justifyContent:'space-between',
        paddingHorizontal:10,   
    }
})
