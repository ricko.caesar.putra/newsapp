import React from 'react'
import { Dimensions, Image, StyleSheet, Text, View } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import { colors } from '../utils'

const { width } = Dimensions.get('window')
const PromoItemsSub = (props) => {
    return (
        <View style={styles.wrapper}>
            <Image source={props.image} style={styles.PromoImage} />
            <View style={styles.textPromo}>
                <Text style={{ fontWeight: 'bold', fontSize: 15 }}>
                    {props.text}
                </Text>
            </View>
            <View style={{margin:5,flexDirection:'row'}}>
                <Icon name='ios-calendar' size={15} color="#575757"/>
                <Text style={{marginLeft:8,fontSize:13, color:'#575757'}}>{props.masaBerlaku}</Text>
            </View>
            {
                props.diskon
                    ?
                    <View style={styles.textDiskon}>
                        <Text>
                            {props.diskon}
                        </Text>
                    </View>
                    :
                    <View />
            }

        </View>
    )
}

export default PromoItemsSub

const styles = StyleSheet.create({
    wrapper: {
        backgroundColor: colors.white,
        elevation: 4,
        borderRadius: 8,
        width: width / 2 - 30,
        marginRight: 8,
        marginLeft: 8,
        marginBottom: 8,
        borderColor:colors.default,
        borderWidth:2
    },
    PromoImage: {
        height: width / 2 - 34,
        width: width / 2 - 34,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8
    },
    textPromo: {
        marginLeft: 10, marginVertical: 5
    },
    textDiskon: {
        position: 'absolute',
        top: 4,
        backgroundColor: 'white',
        padding: 4,
        borderRadius: 8,
        left: 4
    }
})
