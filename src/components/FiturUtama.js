import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import FiturUtamaSub from './FiturUtamaSub'
import { ImageTG,ImageBike, ImageCar, ImageFood, ImageHealth,ImageEmoney, ImageMore, ImageHotel, ImageWisata} from '../assets'
import { useNavigation } from '@react-navigation/native'


const FiturUtama = () => {
    const navigation=useNavigation()
    return (
        <View style={styles.wrapperFiturUtama}>
            <FiturUtamaSub image={ImageFood} title="Food" onPress={()=>navigation.navigate("Food")}/>
            <FiturUtamaSub image={ImageBike} title="Bike"/>
            <FiturUtamaSub image={ImageCar} title="Car"/>
            <FiturUtamaSub image={ImageHotel} title="Hotel"/>
            <FiturUtamaSub image={ImageWisata} title="Attraction"/>
            <FiturUtamaSub image={ImageHealth} title="Doctor"/>
            <FiturUtamaSub image={ImageTG} title="Tour Guide"/>
            <FiturUtamaSub image={ImageMore} title="More"/>

        </View>

    )
}

export default FiturUtama

const styles = StyleSheet.create({
    wrapperFiturUtama: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 15,
        paddingHorizontal:10,
        flexWrap:'wrap',
        width:'100%'
    },

})
