import React from 'react'
import { Image, StyleSheet, Text,TouchableOpacity } from 'react-native'


// const FiturUtamaSub = (props) => {
//     return (
//         <View style={{width:"25%",alignItems:"center"}}>
//             <Image style={styles.imageFiturUtama} source={props.image} />
//             <Text style={styles.textFiturUtama}>{props.title}</Text>
//         </View>
//     )
// }

class FiturUtamaSub extends React.Component {
    render() {
        return (
            <TouchableOpacity style={{ width: "25%", alignItems: "center" }} onPress={this.props.onPress}>
                <Image style={styles.imageFiturUtama} source={this.props.image} />
                <Text style={styles.textFiturUtama}>{this.props.title}</Text>
            </TouchableOpacity>

        )
    }
}

export default FiturUtamaSub

const styles = StyleSheet.create({
    imageFiturUtama: {
        height: 40,
        width: 40,
        marginTop: 15
    },
    textFiturUtama: {
        textAlign: "center",
        marginTop: 4
    }
})
