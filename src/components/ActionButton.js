import React from 'react'
import { Text, View } from 'react-native'
import { Button } from './index'
import { colors } from '../utils'

const ActionButton = ({ desc, title, onPress }) => {
    return (
        <View style={styles.wrapper.component}>
            <Text style={styles.text.desc}>
                {desc}
            </Text>
            <Button
                title={title}
                onPress={onPress}
            />
        </View>
    )
}

const styles = {
    wrapper: {
        component: {
            marginBottom: 20,
        }
    },
    text: {
        desc: {
            fontSize: 15,
            color: 'white',
            fontWeight:'bold',
            textAlign: 'center',
            paddingHorizontal: '15%',
            marginBottom: 2,
            width: 320
        }
    }
}

export default ActionButton