import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import { ImagePay, ImageReward, ImageTopup } from '../assets'


const PaymentComponents = () => {
    return (
        <View style={styles.wrapperPay}>
            <View>
                <Image style={styles.FeatureImage} source={ImagePay} />
                <Text style={{ alignSelf: 'center', marginTop: 5 }}>Pay</Text>
            </View>
            <View>
                <Image style={styles.FeatureImage} source={ImageTopup} />
                <Text style={{ alignSelf: 'center', marginTop: 5 }}>Top up</Text>
            </View>
            <View>
                <Image style={styles.FeatureImage} source={ImageReward} />
                <Text style={{ alignSelf: 'center', marginTop: 5 }}>reward</Text>
            </View>
        </View>
    )
}

export default PaymentComponents

const styles = StyleSheet.create({
    wrapperPay: {
        flexDirection: "row",
        justifyContent: "space-between",

    },
    FeatureImage: {
        height: 45,
        width: 45,
        marginTop: 17,
        marginHorizontal: 40
    }
})
