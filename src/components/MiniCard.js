import React from 'react';
import { StyleSheet, Text, View, Image, Dimensions, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Ionicons'
import moment from 'moment';
import { colors } from '../utils';

const { width, height } = Dimensions.get('screen');

const MiniCard = (props) => {
    const navigation = useNavigation();

    return (
        <TouchableOpacity
            onPress={() => navigation.navigate("DisplayWeb", { url: props.url, title: props.title })}
        >
            <View style={styles.wrapper}>
                <View style={{ flexDirection: 'row' }}>
                    <Image
                        source={{ uri: props.imageMC }}
                        style={styles.imageStyle} />
                    <View style={{ justifyContent: 'space-between' }}>
                        <Text style={{
                            fontSize: 20,
                            width: Dimensions.get("screen").width / 2,
                            color: 'black',
                            fontWeight: 'bold'
                        }}
                            ellipsizeMode="tail"
                            numberOfLines={3}
                        >{props.title}</Text>
                        <Text style={{ fontSize: 12, color: 'black' }}>{props.source}</Text>
                        <View style={{ flexDirection: 'row', marginTop: 10, paddingLeft: 3, justifyContent: 'space-around' }}>
                            <Text>index: {props.index}  </Text>
                            <Icon name='md-calendar' size={15} color={colors.default} />
                            <Text style={{ fontSize: 12, color: 'black', marginLeft: 5 }}>{moment(props.publishedAt, "YYYYMMDD").fromNow()}</Text>
                        </View>
                    </View>
                </View>

                <Text
                    style={{ fontSize: 13, color: 'black', padding: 5, fontWeight: 'bold' }}
                    ellipsizeMode="tail"
                    numberOfLines={3}
                >
                    {props.description}
                </Text>
            </View>

        </TouchableOpacity>
    )
}

export default MiniCard

const styles = StyleSheet.create({
    wrapper: {
        margin: width*0.01,
        marginBottom: 0,
        alignContent: 'center',
        borderColor: colors.default,
        borderWidth: 3,
        borderRadius: 10,
        backgroundColor: 'white',
        elevation: 8,
        width: width*0.98,
        height:height*0.25

    },
    imageStyle: {
        width: 170,
        height: 108,
        margin: 5,
        borderRadius: 10
    }

})