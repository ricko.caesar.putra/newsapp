import React, { Component } from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { View } from 'react-native'
import { createStackNavigator } from "@react-navigation/stack";
import { DisplayWeb, SearchNews,Splash, Login, Register, WelcomeAuth, Admin, DetailUser, EditUser, AdminJSONServer, Account, Home, News, VideoPage, Payment,Food } from '../pages';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs'
import { colors } from '../utils';
import Icon from 'react-native-vector-icons/Ionicons'

const MaterialBottom = createMaterialBottomTabNavigator();
const Stack = createStackNavigator();

const HomeStack = () => {
    return (
        <Stack.Navigator initialRouteName="Splash">
            <Stack.Screen
                name="Splash"
                component={Splash}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="BotomTab"
                component={BotomTab}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="AdminJSONServer"
                component={AdminJSONServer}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name="Admin"
                component={Admin}
                options={{ title: 'Admin using FIREBASE' }}
            />
            <Stack.Screen
                name="DetailUser"
                component={DetailUser}
                options={{ title: 'Detail User' }}
            />
            <Stack.Screen
                name="EditUser"
                component={EditUser}
                options={{ title: 'Edit User' }}
            />

            <Stack.Screen
                name="Login"
                component={Login}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="Register"
                component={Register}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="WelcomeAuth"
                component={WelcomeAuth}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="Food"
                component={Food}
                options={{
                    headerShown: false,
                }}
                
            />
            <Stack.Screen
                name="SearchNews"
                component={SearchNews}
                options={{
                    headerShown: false,
                }}
                
            />
            <Stack.Screen
                name="DisplayWeb"
                component={DisplayWeb}
                options={{
                    headerShown: false,
                }}
                
            />
        </Stack.Navigator>
    )
}

const BotomTab = () => {
    return (
        <MaterialBottom.Navigator
            shifting={false}
            // initialRouteName="Home"
            barStyle={{
                backgroundColor: "white",
                borderWidth: .5,
                borderColor: colors.disable
            }}
            activeColor={colors.default}
            inactiveColor= 'black'
        >
            <MaterialBottom.Screen name="Home" component={Home}
                options={{
                    tabBarLabel: 'Home',
                    tabBarIcon: ({ color }) => (
                        <View style={{ marginTop: -2, width: 30, height: 30, backgroundColor: color, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                            {/* <Image
                                source={ImageHome}
                                style={{ width: 20, height: 20 }}
                            /> */}
                            <Icon name='ios-home' size={20} color='white'/>
                        </View>
                    )
                }}
            />

            <MaterialBottom.Screen name="News" component={News} options={{
                tabBarLabel: 'News',
                tabBarIcon: ({ color }) => (
                    <View style={{ marginTop: -2, width: 30, height: 30, backgroundColor: color, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                        {/* <Image
                            source={ImageActivity}
                            style={{ width: 20, height: 20 }}
                        /> */}
                        <Icon name='ios-list' size={20} color='white'/>
                    </View>
                )
            }} />
            <MaterialBottom.Screen name="SearchNews" component={SearchNews} options={{
                tabBarLabel: 'Search News',
                tabBarIcon: ({ color }) => (
                    <View style={{ marginTop: -2, width: 30, height: 30, backgroundColor: color, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                        {/* <Image
                            source={ImagePayment}
                            style={{ width: 20, height: 20 }}
                        /> */}
                        <Icon name='ios-search' size={20} color='white'/>
                    </View>
                )
            }}
            />
            <MaterialBottom.Screen name="VideoPage" component={VideoPage} options={{
                tabBarLabel: 'Video',
                tabBarIcon: ({ color }) => (
                    <View style={{ marginTop: -2, width: 30, height: 30, backgroundColor: color, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                        {/* <Image
                            source={ImageInbox}
                            style={{ width: 15, height: 15 }}
                        /> */}
                        <Icon name='ios-play' size={20} color='white'/>
                    </View>
                ),
            }} />
            <MaterialBottom.Screen name="Account" component={Account} options={{
                tabBarLabel: 'Account',
                tabBarIcon: ({ color }) => (
                    <View style={{ marginTop: -2, width: 30, height: 30, backgroundColor: color, borderRadius: 15, alignItems: 'center', justifyContent: 'center' }}>
                        {/* <Image
                            source={ImageAccount}
                            style={{ width: 20, height: 20 }}
                        /> */}
                        <Icon name='ios-person' size={20} color='white'/>
                    </View>
                ),
                tabBarBadge: 1,
                tabBarColor: "red",
            }}
            />
        </MaterialBottom.Navigator>
    )

}


export default class Router extends Component {
    render() {
        return (
            <NavigationContainer>
                <HomeStack />
            </NavigationContainer>
        )
    }
}